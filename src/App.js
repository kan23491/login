import React from 'react';
// import './App.css';
import './App.less';
import logo from './imges/logo.svg';
import hug from './imges/hug.png';
import { Row, Col } from 'antd';
import { MailOutlined, LoginOutlined,LockOutlined, CopyrightOutlined  } from '@ant-design/icons';
import { Form, Input, Button,Layout } from 'antd';

function App() {
  const { Footer } = Layout;
  return (
    <Layout >
      <Row justify="end" className="row">
        <Col span={8} className="backgroud"  xs={{span:0}} md={{span:8}} xl={{span: 7}}>
          <img src={logo} alt='logo' className="logo"/>
        </Col>
        <Col span={16} className="main"  xs={{span:24}} md={{span:16}} xl={{span:17}}>
          <div className="header">
            <img src={hug} alt='hug' style={{width:'20%'}}/>
          </div>
          <Form className="formInput">
            <Form.Item >
                <Input  addonAfter={<MailOutlined />} value="james@renewableenergyh.com"/>
            </Form.Item>
            <Form.Item>
              <Input.Password visibilityToggle={false} addonAfter={<LockOutlined  />} />
            </Form.Item>
          </Form> 
          <Button htmlType="submit" icon={<LoginOutlined />}  shape="round" className="btnLogin">
            Login
          </Button>
        </Col>
      </Row>
      <Footer className="footer">
       <p>Copyright <CopyrightOutlined /> 2020 Renewable Energy Hub. All Rights Reserved.</p>
      </Footer>
    </Layout>
  )
}

export default App;
